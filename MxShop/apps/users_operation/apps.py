from django.apps import AppConfig


class UsersOperationConfig(AppConfig):
    name = 'users_operation'
    verbose_name = "操作管理"

