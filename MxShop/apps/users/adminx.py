__author__ = 'leon'
import xadmin
from xadmin import views
from .models import VerifyCode


class BaseSetting(object):
    # 添加主题功能
    enable_themes = True
    use_bootswatch = True

class GlobalSettings(object):
    #全局·配置，后台管理标题和脚本
    site_title = "leon的商城"
    site_footer = "https://www.cnblogs.com/lpdeboke/"

class VerifyCodeAdmin(object):
    list_display = ['code','mobile','add_time']


xadmin.site.register(VerifyCode, VerifyCodeAdmin)
xadmin.site.register(views.BaseAdminView, BaseSetting)
xadmin.site.register(views.CommAdminView, GlobalSettings)

