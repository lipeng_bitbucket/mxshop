from django.views.generic import View
from goods.models import Goods
# from django.http import HttpResponse
# from django.forms.models import model_to_dict
from django.core import serializers
from  django.http import JsonResponse

import json

class GoodListView(View):
    def get(self,request):
        #通过django的view 实现商品别表页
        json_list = []
        #获取所有商品
        goods = Goods.objects.all()
        # for good in goods:
        #     json_dict = {}
        #     json_dict['name'] = good.name
        #     json_dict['category'] = good.category
        #     json_dict['market_price'] = good.market_price
        #     json_list.append(json_dict)
        # for good in goods:
        #     json_dict = model_to_dict(good)
        #     json_list.append(json_dict)
        #     #返回json，一定要指定类型 content_type='application/json'
        # return HttpResponse(json.dumps(json_list),content_type='application/json')
        json_data = serializers.serialize('json',goods)
        json_data = json.loads(json_data)
        return JsonResponse (json_data,safe=False)