import requests


class YunPian(object):
    def __init__(self,api__key):
        self.api_key = api__key
        self.single_send_url = 'https://sms.yunpian.com/v2/sms/single_send.json'

    def send_sms(self,code,mobile):
        parmas = {
            'apikey':self.api_key,
            'mobile':mobile,
            'text':"【李鹏】您的验证码是{code}，如非本人操作请自动忽略".format(code=code)
        }
        response = requests.post(self.single_send_url,data=parmas)
        import json
        re_dict = json.loads(response.text)
        return re_dict

if __name__ == '__main__':
    yunpian = YunPian("2619b2034ef4979b49f712576aac1805")
    yunpian.send_sms('2017','17310198083')