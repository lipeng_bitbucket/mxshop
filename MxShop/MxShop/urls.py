"""MxShop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""


from django.urls import path,include,re_path
import xadmin
from django.views.static import serve
from MxShop.settings import MEDIA_ROOT
# from goods.view_base import GoodsListView

from rest_framework.documentation import include_docs_urls
from goods.views import GoodsListViewSet,CategoryViewSet
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views
from rest_framework_jwt.views import obtain_jwt_token
from users.views import SmsCodeViewset,UserViewset
from users_operation.views import UserFavViewset,LeavingMessageViewset,AddressViewset
from trade.views import ShoppingCartViewset,OrderViewset


router = DefaultRouter()

#配置goods的url
router.register(r'goods', GoodsListViewSet,base_name='goods')
#配置categorys的url
router.register(r'categorys', CategoryViewSet,base_name='categorys')
# 配置code的url
router.register(r'code',SmsCodeViewset,base_name='code')
# 配置code的url
router.register(r'users',UserViewset,base_name='users')
# 配置收藏的url
router.register(r'userfavs',UserFavViewset,base_name='userfavs')
#配置用户留言的url
router.register(r'messages',LeavingMessageViewset,base_name='messages')
#配置收货地址
router.register(r'address',AddressViewset , base_name="address")
#配置购物车的url
router.register(r'shopcarts', ShoppingCartViewset, base_name="shopcarts")#配置订单的url
#配置订单的url
router.register(r'orders',OrderViewset,base_name='orders')

urlpatterns = [
    path('xadmin/', xadmin.site.urls),
    path('api-auth/',include('rest_framework.urls')),
    path('ueditor/',include('DjangoUeditor.urls' )),
    #文件
    path('media/<path:path>',serve,{'document_root':MEDIA_ROOT}),
    #drf文档，title自定义
    path('docs/',include_docs_urls(title='leon的生鲜超市')),
    #商品列表页
    path('', include(router.urls)),
    #drf自带的token认证模式
    path(r'api-token-auth/', views.obtain_auth_token),
    #jwt的token认证接口
    path(r'login/', obtain_jwt_token),

]
